#default values:
    - delay: 3000
    - corner: 'rightTop'
    - type:'danger',
    - templateUrl: 'angular-toast.html' (from $templateCache)


#castom options values:
    type:
        - error
        - warning
        - success
        - info

    corner:
        - leftTop
        - leftBottom
        - rightTop
        - rightBottom

    message:
        {text message, you can use HTML}

    title:
        {toast title you can use HTML}

    delay:
        {delay before toast close}

    templateUrl:
        {url to source of template }

    scope:
        {if you want use custom angular functions with custom template, you can return $scope to toast}



#toast settings examples:

    -  toaster("message text")

    -   toaster.show({message:'message text', type:'success', delay:5000, corner:'leftTop'});

    -  toaster.show({message: '<b>toast message with html</b>', type:'error', title: '<i>Html</i> '});

    -  toaster.show({message: "Just message", templateUrl: "custom_template.html", corner:'rightBottom', type:'info', scope: $scope});


