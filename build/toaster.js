angular.module('toaster', []);
angular.module('toaster').provider('toaster', function () {
    this.options = {
        delay: 3000,
        startTop: 10,
        startRight: 10,
        verticalSpacing: 10,
        horizontalSpacing: 10,
        corner: 'rightTop',
        type:'warning',
        replaceMessage: true,
        templateUrl: 'angular-toast.html'
    };
    this.setOptions = function (options) {
        if (!angular.isObject(options)) throw new Error("Options should be an object!");
        this.options = angular.extend({}, this.options, options);
    };
    this.$get = ["$timeout", "$http", "$compile", "$templateCache", "$rootScope", "$injector", "$sce", "$q", "$window", function ($timeout, $http, $compile, $templateCache, $rootScope, $injector, $sce, $q, $window) {

        var options = this.options;
        var startTop = options.startTop;
        var startRight = options.startRight;
        var verticalSpacing = options.verticalSpacing;
        var horizontalSpacing = options.horizontalSpacing;
        var delay = options.delay;
        var positionY = 'top';
        var positionX = 'right';


        var messageElements = [];
        var isResizeBound = true;

        var toast = function (args) {
            var deferred = $q.defer();

            if (typeof args !== 'object') {
                args = {message: args};
            }

            args.scope = args.scope ? args.scope : $rootScope;
            args.corner = args.corner ? args.corner : options.corner;
            args.type = args.type ? args.type : options.type;
            args.template = args.templateUrl ? args.templateUrl : options.templateUrl;
            args.delay = !angular.isUndefined(args.delay) ? args.delay : delay;
            args.replaceMessage = args.replaceMessage ? args.replaceMessage : options.replaceMessage;
            switch (args.corner) {
                case 'rightTop':
                    positionY = 'top';
                    positionX = 'right';
                    break;

                case 'rightBottom':
                    positionY = 'bottom';
                    positionX = 'right';
                    break;

                case 'leftTop':
                    positionY = 'top';
                    positionX = 'left';
                    break;

                case 'leftBottom':
                    positionY = 'bottom';
                    positionX = 'left';
                    break;


                default:
                    positionY = 'top';
                    positionX = 'right';
                    break;
            }
            $http.get(args.template, {cache: $templateCache}).success(function (template) {

                var scope = args.scope.$new();
                scope.message = $sce.trustAsHtml(args.message);
                scope.title = $sce.trustAsHtml(args.title);
                scope.type  = args.type;
                scope.delay = args.delay;

                var calcPos = function () {
                    var j = 0;
                    var k = 0;
                    var lastTop = startTop;
                    var lastRight = startRight;
                    var lastPosition = [];
                    for (var i = messageElements.length - 1; i >= 0; i--) {
                        var element = messageElements[i];
                        if (args.replaceMessage && i < messageElements.length - 1) {
                            element.addClass('destroy');
                            continue;
                        }
                        var elHeight = parseInt(element[0].offsetHeight);
                        var elWidth = parseInt(element[0].offsetWidth);
                        var position = lastPosition[element._positionY + element._positionX];

                        if ((top + elHeight) > window.innerHeight) {
                            position = startTop;
                            k++;
                            j = 0;
                        }

                        var top = (lastTop = position ? (j === 0 ? position : position + verticalSpacing) : startTop);
                        var right = lastRight + (k * (horizontalSpacing + elWidth));

                        element.css(element._positionY, top + 'px');
                        if (element._positionX == 'center') {
                            element.css('left', parseInt(window.innerWidth / 2 - elWidth / 2) + 'px');
                        } else {
                            element.css(element._positionX, right + 'px');
                        }
                        lastPosition[element._positionY + element._positionX] = top + elHeight;
                        j++;
                    }
                };

                var templateElement = $compile(template)(scope);
                templateElement._positionY = positionY;
                templateElement._positionX = positionX;
                templateElement.addClass(args.type);
                templateElement.bind('webkitTransitionEnd oTransitionEnd otransitionend transitionend msTransitionEnd click', function (e) {
                    e = e.originalEvent || e;
                    if (e.type === 'click' || (e.propertyName === 'opacity' && e.elapsedTime >= 1)) {
                        templateElement.remove();
                        messageElements.splice(messageElements.indexOf(templateElement), 1);
                        calcPos();
                    }
                });
                if (angular.isNumber(args.delay)) {
                    $timeout(function () {
                        templateElement.addClass('killed');
                    }, args.delay);
                }

                angular.element(document.getElementsByTagName('body')).append(templateElement);
                var offset = -(parseInt(templateElement[0].offsetHeight) + 50);
                templateElement.css(templateElement._positionY, offset + "px");
                messageElements.push(templateElement);

                scope._templateElement = templateElement;

                scope.kill = function (isHard) {
                    if (isHard) {
                        messageElements.splice(messageElements.indexOf(scope._templateElement), 1);
                        scope._templateElement.remove();
                        $timeout(calcPos);
                    } else {
                        scope._templateElement.addClass('killed');
                    }
                };

                $timeout(calcPos);

                if (!isResizeBound) {
                    angular.element($window).bind('resize', function (e) {
                        $timeout(calcPos);
                    });
                    isResizeBound = true;
                }

                deferred.resolve(scope);

            }).error(function (data) {
                throw new Error('Template (' + args.template + ') could not be loaded. ' + data);
            });

            return deferred.promise;
        };
        toast.show = function(args){
            return this(args);
        };
        toast.clearAll = function () {
            angular.forEach(messageElements, function (element) {
                element.addClass('destroy');
            });
        };
        return toast;
    }];
});
angular.module("toaster").run(["$templateCache", function ($templateCache) {
    $templateCache.put("angular-toast.html",
        "<div class=\"toast\">" +
        "<h3 ng-show=\"title\" ng-bind-html=\"title\"></h3>" +
        "<div class=\"message\" ng-bind-html=\"message\"></div>" +
        "</div>");
}]);